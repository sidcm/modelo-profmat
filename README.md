# Modelo LaTeX PROFMAT não oficial

Este repositório contém os arquivos disponibilizados pelo programa de
Mestrado Profissional em Matemática em Rede Nacional (PROFMAT) do
ICMC/USP. Algumas alterações foram realizadas para retirada dos
pacotes para apresentação de algoritmos e códigos.

## Aviso importante

Este não é o pacote oficial disponibilizado pelo programa de Mestrado
 Profissional em Matemática em Rede Nacional do ICMC/USP, o oficial está
 disponível na página [para alunos
 PROFMAT](https://www.icmc.usp.br/pos-graduacao/profmat/para-alunos).
 Se você tiver a necessidade de inserir algoritmo ou código de
 programa na dissertação, **não** utilize este pacote, mas o oficial.

## Utilização

Faça o download do [arquivo
zipado](https://gitlab.uspdigital.usp.br/sidcm/modelo-profmat/-/archive/master/modelo-profmat-master.zip)
contendo todos os arquivos necessários para a compilação do projeto. O
arquivo principal a ser compilado chama-se `main.tex`.

## Requisições

Para efetuar pedidos de alteração do conteúdo dos arquivos ou dar
sugestões de melhoria, por favor abra uma [requisição neste
repositório](https://gitlab.uspdigital.usp.br/sidcm/modelo-profmat/issues/new)
mesmo.
